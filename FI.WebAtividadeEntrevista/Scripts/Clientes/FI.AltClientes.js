﻿
$(document).ready(function () {
    if (obj) {
        $('#formCadastro #Nome').val(obj.Nome);
        $('#formCadastro #CEP').val(obj.CEP);
        $('#formCadastro #Email').val(obj.Email);
        $('#formCadastro #Sobrenome').val(obj.Sobrenome);
        $('#formCadastro #Nacionalidade').val(obj.Nacionalidade);
        $('#formCadastro #Estado').val(obj.Estado);
        $('#formCadastro #Cidade').val(obj.Cidade);
        $('#formCadastro #Logradouro').val(obj.Logradouro);
		$('#formCadastro #Telefone').val(obj.Telefone);
		$('#formCadastro #CPF').val(obj.CPF);
    }

    $('#formCadastro').submit(function (e) {
        e.preventDefault();
        
        $.ajax({
            url: urlPost,
            method: "POST",
            data: {
                "NOME": $(this).find("#Nome").val(),
                "CEP": $(this).find("#CEP").val(),
                "Email": $(this).find("#Email").val(),
                "Sobrenome": $(this).find("#Sobrenome").val(),
                "Nacionalidade": $(this).find("#Nacionalidade").val(),
                "Estado": $(this).find("#Estado").val(),
                "Cidade": $(this).find("#Cidade").val(),
                "Logradouro": $(this).find("#Logradouro").val(),
				"Telefone": $(this).find("#Telefone").val(),
				"CPF": $(this).find("#CPF").val(),
				"ListaBeneficiarios": JSON.parse(localStorage.getItem('beneficiario'))
            },
            error:
            function (r) {
                if (r.status == 400)
                    ModalDialog("Ocorreu um erro", r.responseJSON);
                else if (r.status == 500)
                    ModalDialog("Ocorreu um erro", "Ocorreu um erro interno no servidor.");
            },
            success:
            function (r) {
				ModalDialog("Sucesso!", r)
								
                $("#formCadastro")[0].reset();                                
                window.location.href = urlRetorno;
            }
        });
    })
    
})

function ConsultaBeneficiario() {

	var urlPost = window.location.pathname;
	var dados = urlPost.split('/');
	var id = dados[3];

	$.ajax({
		url: '/Cliente/Beneficiario/' + id,
		method: "POST",
		contentType: "application/json; charset=utf-8",		
		error:
			function (response) {
				if (r.status == 400) {
					ModalDialog("Ocorreu um erro", r.responseJSON);
					console.log(response);
				}

				else if (r.status == 500) {
					ModalDialog("Ocorreu um erro", "Ocorreu um erro interno no servidor.");
					console.log(response);
				}

			},
		success:
			function (response) {
				
				if (response.success) {

					for (var i = 0; i < response.dados.length; i++) {

						var jsonObj = JSON.parse(localStorage.getItem('beneficiario'));
						var json = { "cpf": response.dados[i].CPF, "nome": response.dados[i].Nome };

						if (jsonObj != undefined) {							
							jsonObj.push(json);
						} else {
							jsonObj = [json];
						}
						localStorage.setItem('beneficiario', JSON.stringify(jsonObj));
					}
				}
			}
	});
}

function ModalDialog(titulo, texto) {
    var random = Math.random().toString().replace('.', '');
    var texto = '<div id="' + random + '" class="modal fade">                                                               ' +
        '        <div class="modal-dialog">                                                                                 ' +
        '            <div class="modal-content">                                                                            ' +
        '                <div class="modal-header">                                                                         ' +
        '                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>         ' +
        '                    <h4 class="modal-title">' + titulo + '</h4>                                                    ' +
        '                </div>                                                                                             ' +
        '                <div class="modal-body">                                                                           ' +
        '                    <p>' + texto + '</p>                                                                           ' +
        '                </div>                                                                                             ' +
        '                <div class="modal-footer">                                                                         ' +
        '                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>             ' +
        '                                                                                                                   ' +
        '                </div>                                                                                             ' +
        '            </div><!-- /.modal-content -->                                                                         ' +
        '  </div><!-- /.modal-dialog -->                                                                                    ' +
        '</div> <!-- /.modal -->                                                                                        ';

    $('body').append(texto);
    $('#' + random).modal('show');
}

function ModalBeneficiario(texto) {
	$('#modal-mensagem').modal('show');

	CarregaModalBeneficiario();
}


function InsereBeneficiario() {

	var cpf = $('#modalCPF').val();
	var nome = $('#modalNome').val();

	if (cpf != "" && nome != "") {

		if (ValidaCPF(RetiraMascara(cpf))) {

			var jsonObj = JSON.parse(localStorage.getItem('beneficiario'));
			var json = { "cpf": cpf, "nome": nome };

			if (jsonObj != undefined) {

				var found = jsonObj.find(x => x.cpf == cpf);

				if (found != undefined) {

					ModalDialog("Ocorreu um erro", "Já existe um CPF cadastrado.");

					return;
				}

				jsonObj.push(json);

			} else {
				jsonObj = [json];
			}

			localStorage.setItem('beneficiario', JSON.stringify(jsonObj));

			CarregaModalBeneficiario();

		} else {
			ModalDialog("Ocorreu um erro", "CPF informado não é válido.");
		}
	}
}

function CarregaModalBeneficiario() {
	var jsonObj = JSON.parse(localStorage.getItem('beneficiario'));
	$('#tabela_beneficiarios').children("table>tr").remove();

	if (jsonObj != undefined) {
		for (i = 0; i < jsonObj.length; i++) {
			$('#tabela_beneficiarios').append('<tr><td class= "col-md-5">' + jsonObj[i].cpf + '</td ><td class="col-md-7">' + jsonObj[i].nome + '<button style=" margin-left: 5px" type="button" class="btn btn-primary pull-right">Excluir</button><button type="button" class="btn btn-primary pull-right">Alterar</button></td></tr >"');
		}
	}
	$('#modalCPF').val('');
	$('#modalNome').val('');
}

function ValidaCPF(strCPF) {
	var Soma;
	var Resto;
	Soma = 0;
	var controle = 0;
	var primeiro = strCPF.substr(0, 1);

	for (var i = 0; i < strCPF.length; i++) {

		var teste = strCPF[i];

		if (strCPF[i] != primeiro) controle = 1;
	}
	if (controle == 1) {
		if (strCPF == "00000000000") return false;

		for (i = 1; i <= 9; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
		Resto = (Soma * 10) % 11;

		if ((Resto == 10) || (Resto == 11)) Resto = 0;
		if (Resto != parseInt(strCPF.substring(9, 10))) return false;

		Soma = 0;
		for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
		Resto = (Soma * 10) % 11;

		if ((Resto == 10) || (Resto == 11)) Resto = 0;
		if (Resto != parseInt(strCPF.substring(10, 11))) return false;
		return true;
	}
	else {
		return false;
	}
}

function RetiraMascara(ObjCPF) {
	return ObjCPF.replace(/\D+/g, '');
}

//Método formatacaoCampo() não criado por mim, retirado deste site https://fabiobmed.com.br/2012/07/16/excelente-codigo-para-mascara-e-validacao-de-cnpj-cpf-cep-data-e-telefone/
function formatacaoCampo(campo, Mascara, evento) {
	var boleanoMascara;

	var Digitato = evento.keyCode;
	exp = /\-|\.|\/|\(|\)| /g
	campoSoNumeros = campo.value.toString().replace(exp, "");

	var posicaoCampo = 0;
	var NovoValorCampo = "";
	var TamanhoMascara = campoSoNumeros.length;;

	if (Digitato != 8) { // backspace 
		for (i = 0; i <= TamanhoMascara; i++) {
			boleanoMascara = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
				|| (Mascara.charAt(i) == "/"))
			boleanoMascara = boleanoMascara || ((Mascara.charAt(i) == "(")
				|| (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " "))
			if (boleanoMascara) {
				NovoValorCampo += Mascara.charAt(i);
				TamanhoMascara++;
			} else {
				NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
				posicaoCampo++;
			}
		}
		campo.value = NovoValorCampo;
		return true;
	} else {
		return true;
	}
}

function valoresInteiro() {
	if (event.keyCode < 48 || event.keyCode > 57) {
		return false;
	}
	return true;
}

function MascaraCPF(cpf) {
	if (valoresInteiro(cpf) == false) {
		return false;
	}
	return formatacaoCampo(cpf, '000.000.000-00', event);
}